import { createStore } from "vuex";

export default createStore({
    state: {
        globalURL: "https://inaseliesay.gitlab.io/vue-shop/"
    },
    mutations: {},
    actions: {},
    getters: {
        globalURL: state => state.globalURL,
    },
    modules: {},
});