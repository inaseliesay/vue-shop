import { createApp } from 'vue'
import App from './App.vue'
import store from "@/store";
import router from "@/router";

//https://inaseliesay.gitlab.io/vue-shop

createApp(App).use(store).use(router).mount('#app')
