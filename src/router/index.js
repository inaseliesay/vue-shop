import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@/Pages/HomePage";
import AboutPage from "@/Pages/AboutPage";
import ContactPage from "@/Pages/ContactPage";

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomePage
    },
    {
        path: '/about',
        name: 'about',
        component: AboutPage
    },
    {
        path: '/contact',
        name: 'contact',
        component: ContactPage
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior(to,from, savedPosition){
        return savedPosition || new Promise((resolve => {
            setTimeout(() => resolve({top: 0}),300)
        }))
    },
})

export default router;